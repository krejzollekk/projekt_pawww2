/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfik;

import java.util.ArrayList;
import java.util.List;
import models.Danie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author krejz
 */
public class DanieBean {

ArrayList<Danie> dania =new ArrayList<>();

public DanieBean() {

}

public int getCountDania(){
    List<Danie> temp =  getDania();
    return temp.size();
}


public List<Danie> getDania(){

SessionFactory sf = NewHibernateUtil.getSessionFactory();

Session session=sf.openSession();
dania=(ArrayList<Danie>)session.createQuery("from Danie").list();
session.close();
return dania;
}

}
