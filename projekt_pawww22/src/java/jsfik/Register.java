/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfik;

import api.Authenticator;
import javax.faces.component.html.*;
import javax.servlet.http.HttpSession;
import models.User;

/**
 *
 * @author krejz
 */
public class Register {
    private HtmlForm registerForm;
    private HtmlInputText firstname;
    private HtmlInputText lastname;
    private HtmlInputText email;
    private HtmlInputSecret pass;
    private HtmlCommandButton registerButton;

    public void setFirstname(HtmlInputText firstname) {
        this.firstname = firstname;
    }

    public void setEmail(HtmlInputText email) {
        this.email = email;
    }

    public void setRegisterForm(HtmlForm registerForm) {
        this.registerForm = registerForm;
    }

    public void setLastname(HtmlInputText lastname) {
        this.lastname = lastname;
    }

    public void setPass(HtmlInputSecret pass) {
        this.pass = pass;
    }

    public void setRegisterButton(HtmlCommandButton registerButton) {
        this.registerButton = registerButton;
    }

    public HtmlInputText getEmail() {
        return email;
    }

    public HtmlInputText getFirstname() {
        return firstname;
    }

    public HtmlInputText getLastname() {
        return lastname;
    }

    public HtmlInputSecret getPass() {
        return pass;
    }

    public HtmlCommandButton getRegisterButton() {
        return registerButton;
    }

    public HtmlForm getRegisterForm() {
        return registerForm;
    }
    
    
    
    public String doRegister() {
                System.err.println("dotarłem tu");
                String xemail = (String) this.email.getValue();
		String xpassword = (String) this.pass.getValue();
                String xfirstname = (String) this.firstname.getValue();
                String xlastname = (String) this.lastname.getValue();
                System.err.println("jestem tuuuuuuuuuuuu");
                    User temp = new User(xemail, xpassword);
                       temp.setFirstname(xfirstname);
                       temp.setLastname(xlastname);
                       temp.save();
                       System.err.println("jestem zapisany");
       return "success_register";
    }
    
}
