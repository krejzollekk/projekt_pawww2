/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class MyValidator implements Validator{

@Override
public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {

UIInput uiInputPassword= (UIInput) uic.findComponent("pass");

if(!o.toString().equals(uiInputPassword.getLocalValue())){
FacesMessage message = new FacesMessage();
message.setSeverity(FacesMessage.SEVERITY_ERROR);
message.setSummary("Passwords not equal.");
message.setDetail("Both passwords must be equal");
fc.addMessage("confirmpassword", message);
throw new ValidatorException(message);
}
}

}
