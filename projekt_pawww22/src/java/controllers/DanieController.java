/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import api.Authenticator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Danie;
import models.User;

@WebServlet(name = "DanieController", urlPatterns = {"/DanieController"})
public class DanieController extends HttpServlet {

    public DanieController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    //wyświetlenie konkretnego przepisu lub formatka na nowy przepis
        RequestDispatcher rd = null;
        
                        HttpSession session = request.getSession();
                Authenticator auth = new Authenticator(session);
                User logged = auth.isLogged();
                
    
        
        String id_str = request.getParameter("danie_id");
        String edit = request.getParameter("edit");
        int danie_id = 0;
        int edit_int = 0;
            if(id_str != null) danie_id=Integer.parseInt(id_str);
            if(edit != null) edit_int=Integer.parseInt(edit);
            
            
            rd = request.getRequestDispatcher("/danie_details.jsp");
            
            if(logged == null && danie_id == 0)
               rd = request.getRequestDispatcher("/login.jsp");
                
                request.setAttribute("danie_id", danie_id);
                
            Danie temp_danie = null;    
            if(danie_id > 0){
                temp_danie = new Danie();
                temp_danie.findDanie(danie_id);
            }
                request.setAttribute("danie_obj", temp_danie);
                
                if(logged == null) edit_int = 0;
                
                request.setAttribute("edit", edit_int);
               
                
                
                
        rd.forward(request, response);
            
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    //edycja/dodawanie nowego przepisu / zapis do bazy
        request.setCharacterEncoding("utf-8");
        RequestDispatcher rd = null;

    HttpSession session = request.getSession();
                Authenticator auth = new Authenticator(session);
                User logged = auth.isLogged();
                
    if(logged != null){    
        
            String edit_id = request.getParameter("danie_id");
            int id = 0;
            String nazwa = request.getParameter("nazwa");
            String pora_dnia = request.getParameter("pora_dnia");
            String description = request.getParameter("description");
            String skladniki_powiazane = request.getParameter("skladniki_powiazane");
            
                if(edit_id != null) id=Integer.parseInt(edit_id);
            
            Danie temp = new Danie();
                temp.findDanie( id );
                temp.setDescription(description);
                temp.setName(nazwa);
                temp.setPora_dnia(pora_dnia);
            
                temp.setCreator(logged);
                
                temp.save();
                temp.parseSkladniki(skladniki_powiazane);
                temp.save();
                
            rd = request.getRequestDispatcher("/danie_details.jsp");
            System.out.println("controllers.DanieController.doPost() ID: "+temp.getId());
                request.setAttribute("danie_id", temp.getId());
                request.setAttribute("danie_obj", temp);
                request.setAttribute("edit", 0);
    }else{
        rd = request.getRequestDispatcher("/login.jsp");
    }
                
        rd.forward(request, response);       
    }


}