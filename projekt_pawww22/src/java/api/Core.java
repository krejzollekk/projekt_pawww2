/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Danie;
import models.User;
import models.Skladnik;

/**
 *
 * @author krejz
 */
public class Core {
    
    public ArrayList<Danie> ListAllDania(String search, String pora_dnia,int kcal_od, int kcal_do, int page, int limit_per_page){
        ArrayList<Danie> temp_list = new ArrayList<Danie>();
      
        if(search == null) search = "";
        if(pora_dnia == null) pora_dnia = "";
        
        String query_add = "";
        if(pora_dnia.trim().length() > 0)
            query_add = " AND pora_dnia = '"+pora_dnia+"' ";
        
        if(kcal_od >= 0)
            query_add += " AND kalorycznosc >= '"+kcal_od+"' ";
 
        if(kcal_do > 0)
            query_add += " AND kalorycznosc <= '"+kcal_do+"' ";
                
        String query = "Select danie.*, (select count(*) from danie_skladniki where danie_id = danie.id) as ile_skladnikow from danie";
                query += " Where name like '%"+search+"%'"+query_add+" LIMIT "+(page*limit_per_page)+","+limit_per_page+";";

                
               System.out.println("api.Core.ListAllDania() "+query);
            
            Connection con = DBconn.get_connection();
            try{
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()){
                       Danie temp = new Danie();
                    
                    temp.setId(rs.getInt("id"));
                    temp.setDescription(rs.getString("description"));
                    temp.setName(rs.getString("name"));
                    temp.setPora_dnia(rs.getString("pora_dnia"));
                    temp.setKalorycznosc(rs.getInt("kalorycznosc"));
                    temp.setIle_skladnikow(rs.getInt("ile_skladnikow"));
                    
                        User temp_usr = new User();
                        temp_usr.findUser(rs.getInt("creator_id"), "","");
                    temp.setCreator(temp_usr);
                    
                    
                    temp_list.add(temp);
                }
                
            }catch (SQLException e){
                System.out.print("Blad " + e);
            }
        
        return temp_list;
    }
    
    public static int ConvToInt( String str ){
        int number = 0;
        try
        {
            if(str != null)
              number = Integer.parseInt(str);
        }
        catch (NumberFormatException e)
        {
            number = 0;
        }
        return number;
    }
    
        public ArrayList<Skladnik> ListAllSkladniki(String search){
        ArrayList<Skladnik> temp_list = new ArrayList<Skladnik>();
      
        String query = "Select * from skladniki";
                query += " Where name like '%"+search+"%';";
            
            Connection con = DBconn.get_connection();
            try{
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()){
                       Skladnik temp = new Skladnik();
                    
                    temp.setId(rs.getInt("id"));
                    temp.setName(rs.getString("name"));
                    temp.setKalorycznosc(rs.getInt("kalorycznosc"));
                    
                    
                    temp_list.add(temp);
                }
                
            }catch (SQLException e){
                System.out.print("Blad " + e);
            }
        
        return temp_list;
    }
    
    public ArrayList<String> getPoraDnia_options(){
        ArrayList<String> temp = new ArrayList<String>();
                temp.add("sniadanie");
                temp.add("lunch");
                temp.add("obiad");
                temp.add("deser");
                temp.add("podwieczorek");
                temp.add("kolacja");
        
        return temp;
    }
}
