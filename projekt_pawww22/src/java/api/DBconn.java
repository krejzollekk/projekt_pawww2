package api;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author krejz
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconn {

public static DBconn dBTest;

private static Connection con = null;


private DBconn(){}

public static Connection get_connection(){
		if(con == null)
			try_connect();
		return con;
	}

private static void try_connect() {
con = null;
try {
//Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
Class.forName("com.mysql.jdbc.Driver");
//z serwerem Derby: Class.forName("org.apache.derby.jdbc.ClientDriver");

} catch (ClassNotFoundException e) {
System.out.print("Błąd prz ładowaniu sterownikabazy: " + e);
System.exit(-1);
}
try {
//con = DriverManager.getConnection("jdbc:odbc:test");
con = DriverManager .getConnection("jdbc:mysql://localhost:3306/dietetyczny","root","DawidToPedal123."); // git hasełko?ta
//z serwerem Derby: con=DriverManager.getConnection("jdbc:derby://localhost:1527/test","root","root");

} catch (SQLException e) {
System.out.println("Nie mona nawiazac polaczenia z baza danych" + e);
System.exit(-1);
}
System.out.println("Polaczenie nawiazano");
}

public void DBclose() {
try {
con.close();
} catch (SQLException e) {
System.out.print("Blad przy zamykaniu");
System.exit(-1);
}
System.out.print("Polaczenie zamkniete");
}

}