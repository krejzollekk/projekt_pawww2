/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import api.DBconn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author krejz
 */
public class User {
 
        private int id = 0;
        private String firstname = "";
        private String lastname = "";
	private String password = "";
        private String email = "";
        private String role = "user";
        
        
        public User(){
            
        }
        
         public User(String email, String password){
		this.email = email;
		this.password = password;
	}
         
        public void setLastname( String lastname ){
            this.lastname = lastname;
        }
        
        public void setFirstname(String firstname){
            this.firstname = firstname;
        }
        
        public void setEmail( String email ){
            this.email = email;
        }
        
        public String getLastname(){
            return this.lastname;
        }

        public String getFirstname(){
            return this.firstname;
        }
 
        public String getEmail(){
            return this.email;
        }
        
        public int getId(){
            return this.id;
        }
        
	public String getPassword() {
		return password;
	}
 
	public void setPassword(String password) {
		this.password = password;
	}

        public void setRole(String role) {
            this.role = role;
        }

        public String getRole() {
            return role;
        }
 
        
        
        public User findUser(int id, String email, String password){
            String query = "Select * from users";
                
            if(id > 0){
                query += " Where id = '"+id+"' LIMIT 1;";
            }else
                query += " Where email like '"+email+"' AND password = '"+password+"' LIMIT 1;";
            
            
            Connection con = DBconn.get_connection();
            try{
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()){
                this.firstname = rs.getString("firstname");
                this.email = rs.getString("email");
                this.lastname = rs.getString("lastname");
                this.password = rs.getString("password");
                this.id = rs.getInt("id");
                this.role = rs.getString("role");
                }
                
            }catch (SQLException e){
                System.out.print("Blad " + e);
            }
            return this;
        }
        public User findUser1(int id){
            String query = "Select * from users";
                
            if(id > 0){
                query += " Where id = '"+id+"' LIMIT 1;";
            }else
                query += " Where email like '"+email+"' AND password = '"+password+"' LIMIT 1;";
            
            
            Connection con = DBconn.get_connection();
            try{
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()){
                this.firstname = rs.getString("firstname");
                this.email = rs.getString("email");
                this.lastname = rs.getString("lastname");
                this.password = rs.getString("password");
                this.id = rs.getInt("id");
                this.role = rs.getString("role");
                }
                
            }catch (SQLException e){
                System.out.print("Blad " + e);
            }
            return this;
        }
        
        public void save(){
            Connection con = DBconn.get_connection();
            
            if(this.id > 0){
                //update
                String query = "UPDATE users SET firstname = '"+this.firstname+"', lastname = '"+this.lastname+"', email = '"+this.email+"', password = '"+this.password+"' ";
                    query += "WHERE id = '"+this.id+"' LIMIT 1;";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query);
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }else{
                //insert
                String query = "INSERT INTO users (firstname, lastname, email, password) ";
                    query += "VALUES('"+this.firstname+"', '"+this.lastname+"', '"+this.email+"', '"+this.password+"')";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                    ResultSet rs = statement.getGeneratedKeys();
                        rs.next(); 
                    int last_id = rs.getInt(1);
                    
                    this.id = last_id;
                        System.err.println("USER - nowe id: "+last_id);                  
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }
        }
        
        public void remove(){
            Connection con = DBconn.get_connection();
            
             String query = "DELETE FROM users";
                    query += " WHERE id = '"+this.id+"' LIMIT 1;";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query);
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
        }
        
        public String toString(){
            return "[user:   email:"+this.email+"; password:"+this.password+";]";
        }
}