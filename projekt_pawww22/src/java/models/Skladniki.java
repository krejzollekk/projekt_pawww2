/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import api.DBconn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author krejz
 */
public class Skladniki {
    private int id = 0;
    private Skladnik obj_skladnik = null;
    private Danie obj_danie = null;
    private int ilosc = 0;

    public int getId() {
        return id;
    }

    public int getIlosc() {
        return ilosc;
    }

    public Danie getObj_danie() {
        return obj_danie;
    }

    public Skladnik getObj_skladnik() {
        return obj_skladnik;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIlosc(int ilosc) {
        this.ilosc = ilosc;
    }

    public void setObj_danie(Danie obj_danie) {
        this.obj_danie = obj_danie;
    }

    public void setObj_skladnik(Skladnik obj_skladnik) {
        this.obj_skladnik = obj_skladnik;
    }
    
    public void save(){
            Connection con = DBconn.get_connection();
                  
            if(this.id > 0){
                //update
                String query = "UPDATE danie_skladniki SET danie_id ='"+this.obj_danie.getId()+"', skladnik_id='"+this.obj_skladnik.getId()+"',";
                        query += " ilosc='"+this.ilosc+"' ";
                    query += "WHERE id = '"+this.id+"' LIMIT 1;";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query);
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }else{
                //insert
                String query = "INSERT INTO danie_skladniki (danie_id, skladnik_id, ilosc) ";
                    query += "VALUES('"+this.obj_danie.getId()+"', '"+this.obj_skladnik.getId()+"', '"+this.ilosc+"')";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                    ResultSet rs = statement.getGeneratedKeys();
                        rs.next(); 
                    int last_id = rs.getInt(1);
                    
                    this.id = last_id;
                        System.err.println("DANIE_SKŁADNIK - nowe id: "+last_id);                  
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }
        }
}
