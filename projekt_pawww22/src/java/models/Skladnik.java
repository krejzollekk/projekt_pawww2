/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import api.DBconn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author krejz
 */
public class Skladnik {
    private int id = 0;
    private String name = "";
    private int kalorycznosc = 0;

    public int getKalorycznosc() {
        return kalorycznosc;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setKalorycznosc(int kalorycznosc) {
        this.kalorycznosc = kalorycznosc;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Skladnik findSkladnik(int id){
            String query = "Select * from skladniki";
                query += " Where id = '"+id+"' LIMIT 1;";

            
            Connection con = DBconn.get_connection();
            try{
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()){
                    this.id = rs.getInt("id");
                    this.name = rs.getString("name");
                    this.kalorycznosc = rs.getInt("kalorycznosc");
                }
                
            }catch (SQLException e){
                System.out.print("Blad " + e);
            }
            return this;
    }
    
    public void save(){
            Connection con = DBconn.get_connection();
                  
            if(this.id > 0){
                //update
                String query = "UPDATE skladniki SET name ='"+this.name+"', kalorycznosc='"+this.kalorycznosc+"' ";
                    query += "WHERE id = '"+this.id+"' LIMIT 1;";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query);
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }else{
                //insert
                String query = "INSERT INTO skladniki (name, kalorycznosc) ";
                    query += "VALUES('"+this.name+"', '"+this.kalorycznosc+"')";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                    ResultSet rs = statement.getGeneratedKeys();
                        rs.next(); 
                    int last_id = rs.getInt(1);
                    this.id = last_id;
                        System.err.println("SKŁADNIKI - nowe id: "+last_id);                  
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }
        }
        
    public void remove(){
            Connection con = DBconn.get_connection();
            
             String query = "DELETE FROM skladniki";
                    query += " WHERE id = '"+this.id+"' LIMIT 1;";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query);
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
    }
}
