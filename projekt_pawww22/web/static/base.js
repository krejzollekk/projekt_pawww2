/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Kalorycznosc = 0;

function parseTableSkladniki(){
    var temp_result = "";
    Kalorycznosc = 0;
    $(".all_skladniki").each(function() {
        var id = $( this ).attr('name');
        var ilosc = parseInt($("#poz_skladnik_ilosc"+id).val());
        Kalorycznosc += ilosc*parseInt($( this ).text());
        
        temp_result += id+"-"+ilosc+";";
    });
    $("#skladniki_powiazane").val(temp_result);
    $("#kcal_total").text(Kalorycznosc);
}

function remove_skladniki( id ){
    $("#poz_skladnik"+id).remove();
    parseTableSkladniki();
}

function dodaj_skladnik_tabela(){
    var id = $("#skladniki_lista").val();
    var nazwa = $("#s_lista_pos"+id).attr('nazwa');
    var kcal = $("#s_lista_pos"+id).attr('kcal');
    
    if( $("#poz_skladnik"+id).length > 0 ){
        //istnieje taka pozycja juz w liscie
        $("#poz_skladnik_ilosc"+id).val( parseInt($("#poz_skladnik_ilosc"+id).val()) + 1 );
    }else{
        //nie istnieje taka pozycja w liście
        $("<tr id=\"poz_skladnik"+id+"\"><td>"+nazwa+"</td>"+
                "<td name=\""+id+"\" class=\"all_skladniki\">"+kcal+"</td>"+
                "<td><input min=\"1\" onchange=\"parseTableSkladniki()\" id=\"poz_skladnik_ilosc"+id+"\" style=\"width:99%;\" type=\"number\" value=\"1\"/></td>"+
                "<td align=\"center\"><a href=\"#\" onclick=\"remove_skladniki( "+id+" );\">X</a></td></tr>").insertBefore('#tabela_skladniki tr:last-child'); ;
    }
    
    parseTableSkladniki();
}

function dodaj_skladnik_baza(){
    nazwa = $('#nazwa_skladnik');
    kcal = $('#kcal_skladnik');
    
var request = new XMLHttpRequest();
var params = "name="+nazwa.val()+"&kalorycznosc="+kcal.val();

request.open('POST', "SkladnikController", true); //"true" makes the request asynchronous
//Send the proper header information along with the request
request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
request.setRequestHeader("Content-length", params.length);
request.setRequestHeader("Connection", "close");
request.onreadystatechange = function() {
if (request.readyState == 4) {
if (request.status == 200)
{
    var data = request.responseText;
    if(isNaN(data))
            alert(data);
        else{
            $("#skladniki_lista").append("<option id=\"s_lista_pos"+parseInt(data)+"\" nazwa=\""+nazwa.val()+"\" kcal=\""+kcal.val()+"\" value=\""+parseInt(data)+"\">"+nazwa.val()+" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "+kcal.val()+"kcal</option>");
            nazwa.val("");
            kcal.val("");
            
            $("#skladniki_lista").val( parseInt(data) );
            dodaj_skladnik_tabela();
        }
}
else
{
    alert("Request failed :<");
}
}
};
request.send(params);
}