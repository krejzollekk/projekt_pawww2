<%@page import="models.Skladniki"%>
<%@page import="models.Skladnik"%>
<%@page import="api.Core"%>
<%@page import="models.Danie"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>   
    <%@ include file="static/header.jsp" %>
    <div class="box_main_include" style="width:75%; min-height:300px;">
    <%
        int id_dania = (int)request.getAttribute("danie_id");
        int edit = (int)request.getAttribute("edit");
        String nazwa = "Dodaj nowe danie";
        Danie tmp_danie = null;
        if(id_dania > 0 && edit == 0){
            tmp_danie = (Danie)request.getAttribute("danie_obj");
            nazwa = tmp_danie.getName();
    %>
    <div id="container_title"><%=nazwa%></div>
    <div id="container_content">
        <div>Kaloryczność: <b><%=tmp_danie.getKalorycznosc()%></b> | Pora dnia: <b><%=tmp_danie.getPora_dnia()%></b> | Dodał: <b><%=tmp_danie.getCreator_id().getEmail()%></b>
        
            <span style="margin-left: 40px;"> 
                <a href="DanieController?danie_id=<%=id_dania%>&edit=1">EDYTUJ</a>
            </span>
        </div>
    
    <table cellspacing="0" style="width:100%; min-height: 200px; margin-top: 10px;">
  <tr>
      <td valign="top" width="33%" style="border-right: 1px solid #cccccc;"><b>Składniki:</b><br/>
          <div style="min-height: 150px; border-left: 1px solid #cccccc;">
          <table id="tabela_skladniki" class="dania_list small" cellspacing="0" style="margin-bottom: 20px;">
  <tr>
    <td width="64%">Nazwa</td>
    <td width="16%">Kcal</td> 
    <td width="13%">Ilość</td>
    <td width="7%">&nbsp;</td>
  </tr>
<%
for(Skladniki sklad_obj: tmp_danie.getSkladniki_list()){
    Skladnik skladnik = sklad_obj.getObj_skladnik();
%>
<tr id=\"poz_skladnik<%=skladnik.getId()%>\"><td><%=skladnik.getName()%></td>
                <td name="<%=skladnik.getId()%>" class="all_skladniki"><%=skladnik.getKalorycznosc()%></td>
                <td><%=sklad_obj.getIlosc()%></td>
                <td align="center">&nbsp;</td></tr>
<%
}
%>
  <tr>
    <td align="right"> w sumie:</td>
    <td><b><span id="kcal_total"><%=tmp_danie.getKalorycznosc()%></span></b></td> 
    <td></td>
    <td></td>
  </tr>  
</table>
          </div></td>
      <td valign="top" width="67%"><div style="padding: 5px;">
      <b>Opis przygotowania:</b><br/>
        <%=tmp_danie.getDescription()%>
          </div></td> 
  </tr>
    </table>

    
    
    
    </div>
    <% }else{ 
        Core tmp_core = new Core();
        
        String name="",description="",pora_dnia="";

        if(id_dania > 0){
            tmp_danie = (Danie)request.getAttribute("danie_obj");
                name = tmp_danie.getName();
                description = tmp_danie.getDescription();
                pora_dnia = tmp_danie.getPora_dnia();
        }

        Skladnik tx = new Skladnik();
        tx.setKalorycznosc(100);
        tx.setName("Pomidor");
        //tx.save();

    %>
    <div id="container_title">Dodaj/edytuj nowe danie</div>
    <div id="container_content">
    <form action="DanieController" method="post">
        <input type="hidden" name="danie_id" value="<%=id_dania%>" />
        nazwa: <input type="text" style="width: 300px;" name="nazwa" value="<%=name%>"/>
      
        <span style="margin-left: 40px;">pora dnia:  <select size="1" name="pora_dnia">
                        <% for(String pozycja: tmp_core.getPoraDnia_options()){ %>
                        <option value="<%=pozycja%>"><%=pozycja%></option>
                        <% } %>
                    </select>
        </span>
    <table cellspacing="0" style="width:100%; min-height: 200px; margin-top: 10px;">
  <tr>
      <td valign="top" width="33%" style="border-right: 1px solid #cccccc;"><b>Składniki:</b><br/>
          <div style="min-height: 150px; border-left: 1px solid #cccccc;">
          <table id="tabela_skladniki" class="dania_list small" cellspacing="0" style="margin-bottom: 20px;">
  <tr>
    <td width="64%">Nazwa</td>
    <td width="16%">Kcal</td> 
    <td width="13%">Ilość</td>
    <td width="7%">&nbsp;</td>
  </tr>
<%
    if(tmp_danie != null)
for(Skladniki sklad_obj: tmp_danie.getSkladniki_list()){
    Skladnik skladnik = sklad_obj.getObj_skladnik();
%>
<tr id=\"poz_skladnik<%=skladnik.getId()%>\"><td><%=skladnik.getName()%></td>
                <td name="<%=skladnik.getId()%>" class="all_skladniki"><%=skladnik.getKalorycznosc()%></td>
                <td><input min="1" onchange="parseTableSkladniki()" id="poz_skladnik_ilosc<%=skladnik.getId()%>" style="width:99%;" type="number" value="<%=sklad_obj.getIlosc()%>"/></td>
                <td align="center"><a href="#" onclick="remove_skladniki( <%=skladnik.getId()%> );">X</a></td></tr>
<%
}
%>
  <script>
        $( document ).ready(function() {
            parseTableSkladniki();
        });
  </script>
  
  <tr>
    <td align="right"> w sumie:</td>
    <td><b><span id="kcal_total">0</span></b></td> 
    <td></td>
    <td></td>
  </tr>  
</table>
          </div>
          <select id="skladniki_lista" size="1" style="width:80%;">
                    <% for(Skladnik pozycja: tmp_core.ListAllSkladniki("")){ %>
                        <option id="s_lista_pos<%=pozycja.getId()%>" nazwa="<%=pozycja.getName()%>" kcal="<%=pozycja.getKalorycznosc()%>" value="<%=pozycja.getId()%>"><%=pozycja.getName()+" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "+pozycja.getKalorycznosc()%>kcal</option>
                        <% } %>
      </select>
      <input style="width:18%;" type="submit" onclick="dodaj_skladnik_tabela(); return false;" value="Dodaj" />
          <br/>
          <input style="width:47%;" type="text" id="nazwa_skladnik" placeholder="Nazwa składnika..."> 
          <input style="width:27%;" type="text" id="kcal_skladnik" placeholder="Kaloryczność..."> 
      <input style="width:18%;" type="submit" onclick="dodaj_skladnik_baza(); return false;" value="Dodaj" />
      </td>
      <td valign="top" width="67%"><div style="padding: 5px;">
      <b>Opis przygotowania:</b><br/>
      <textarea name="description" style="min-width:90%; max-width: 90%; min-height: 200px;"><%=description%></textarea>
      <input type="hidden" id="skladniki_powiazane" name="skladniki_powiazane" value="">
          </div></td> 
  </tr>
    </table>
    
      <div style="padding-top: 40px;">
    <input type="submit" value="Zapisz zmiany" />
      </div>
    </form>
    </div>
    <% } %>  
    </div>
    <%@ include file="static/footer.jsp" %>